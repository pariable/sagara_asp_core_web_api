﻿using System;
using System.Data;
using System.Reflection.Metadata;
using System.Threading.Tasks;
using MySqlConnector;

namespace StudentAdmissionManagement
{
    public class StudentAdmissionDetailsModel
    {
        public int StudentID { get; set; }
        public string StudentName { get; set; }
        public string StudentClass { get; set; }
        public DateTime DateofJoining { get; set; }

        internal AppDb Db { get; set; }

        
        public async Task InsertAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"INSERT INTO `admission` (`name`, `Class`,`date_of_join`) VALUES (@StudentName, @StudentClass,@DateofJoining);";
            BindParams(cmd);
            await cmd.ExecuteNonQueryAsync();
            StudentID = (int)cmd.LastInsertedId;
        }

        public async Task UpdateAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"UPDATE `admission` SET `name` = @StudentName, `class` = @StudentClass WHERE `Id` = @StudentID;";
            BindParams(cmd);
            BindId(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        public async Task DeleteAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM `admission` WHERE `id` = @StudentID;";
            BindId(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        private void BindId(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@StudentID",
                DbType = DbType.Int32,
                Value = StudentID,
            });
        }

        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@StudentName",
                DbType = DbType.String,
                Value = StudentName,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@StudentClass",
                DbType = DbType.String,
                Value = StudentClass,
            });
        }
    }
}